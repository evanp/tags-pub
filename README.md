# tags.pub

[![pipeline status](https://gitlab.com/evanp/tags-pub/badges/master/pipeline.svg)](https://gitlab.com/evanp/tags-pub/commits/master)

This is the source code for the [tags.pub](https://tags.pub/) service. It provides [hashtag](https://en.wikipedia.org/wiki/Hashtag) objects on the [ActivityPub](https://activitypub.rocks/) network.

## License

Copyright 2017 Evan Prodromou [legal@imfn.me](mailto:legal@imfn.me)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
