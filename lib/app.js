// app.js -- top-level app for tags.pub
//
// Copyright 2017 Evan Prodromou <legal@imfn.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const express = require('express')
const path = require('path')
const favicon = require('serve-favicon')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const {Databank} = require('databank')
const doasync = require('doasync')

const index = require('../routes/index')
const tag = require('../routes/tag')

module.exports = (argv) => {
  const app = express()

  app.argv = argv

  app.makeURL = function (rel) {
    return `${this.argv.root}${rel}`
  }

  app.db = doasync(Databank.get(argv.driver, JSON.parse(argv.params)))

  app.disable('x-powered-by')
  app.set('view engine', 'pug')
  app.set('views', path.join(__dirname, '..', 'views'))
  app.use(logger('dev'))
  app.use(bodyParser.json({type: ['application/activity+json', 'application/ld+json', 'application/json']}))
  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(cookieParser())
  app.use(favicon(path.join(__dirname, '..', 'public', 'images', 'favicon.ico')))
  app.use('/jquery', express.static(path.join(__dirname, '..', 'node_modules', 'jquery', 'dist')))
  app.use('/bootstrap', express.static(path.join(__dirname, '..', 'node_modules', 'bootstrap', 'dist')))
  app.use(express.static(path.join(__dirname, '..', 'public')))

  // add utility method for writing AS2

  app.use((req, res, next) => {
    res.as2 = function (obj) {
      if (req.accepts('application/activity+json')) {
        res.type('application/activity+json')
        res.send(Buffer.from(JSON.stringify(obj), 'utf8'))
      } else if (req.accepts('application/ld+json')) {
        res.type('application/ld+json; profile="https://www.w3.org/ns/activitystreams"')
        res.send(Buffer.from(JSON.stringify(obj), 'utf8'))
      } else if (req.accepts('application/json')) {
        res.json(obj)
      } else {
        res.sendStatus(406)
      }
    }
    next()
  })

  app.use('/', index)
  app.use('/tag', tag)

  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err = new Error('Not Found')
    err.status = 404
    next(err)
  })

  // error handler
  app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    // render the error page
    res.status(err.status || 500)
    res.render('error', {err: err})
  })

  return app
}
