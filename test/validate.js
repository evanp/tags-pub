// validate.js -- validations for types in the tags.pub universe
//
// Copyright 2017 Evan Prodromou <legal@imfn.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows

const AS2 = 'https://www.w3.org/ns/activitystreams'
const SEC = 'https://w3id.org/security/v1'

function document (json) {
  assert.isObject(json, 'Top-level document is not an object')
  assert.include(json, '@context')
  assert.isString(json['@context'], 'Top-level document @context is not a string')
  assert.equal(json['@context'], AS2)
}

function secureDocument (json) {
  assert.isObject(json)
  assert.isArray(json['@context'], 'Context is not an array')
  assert.lengthOf(json['@context'], 2)
  assert.isString(json['@context'][0])
  assert.equal(json['@context'][0], AS2)
  assert.isString(json['@context'][1])
  assert.equal(json['@context'][1], SEC)
}

function announce (json) {
  assert.isObject(json, 'Announce activity is not an object')
  assert.include(json, 'id')
  assert.isString(json.id)
  assert.include(json, 'type')
  assert.isString(json.type)
  assert.equal(json.type, 'Announce')
  assert.isObject(json.object)
  assert.isString(json.object.id)
}

module.exports = {
  document: document,
  secureDocument: secureDocument,
  announce: announce
}
