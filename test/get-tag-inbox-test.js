// get-tag-inbox-test.js -- GET /tag/:tag/inbox
//
// Copyright 2017 Evan Prodromou <legal@imfn.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')

const Server = require('./server')
const env = require('./env')

const AS2_MIME = 'application/activity+json'
const TAG = 'foo'

vows.describe('GET /tag/:tag/inbox')
  .addBatch(Server.batch(env, {
    'and we get the inbox collection of a tag': {
      topic () {
        const root = `http://${env.TAGS_PUB_HOSTNAME}:${env.TAGS_PUB_PORT}`
        const url = `${root}/tag/${TAG}/inbox`
        return fetch(url, {'headers': {'Accept': AS2_MIME}})
      },
      'it gives a 403 Unauthorized response': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
        assert.equal(res.status, 403)
      }
    }
  }))
  .export(module)
