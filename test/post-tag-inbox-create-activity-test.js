// post-tag-inbox-create-activity-test.js -- POST /tag/:tag/inbox
//
// Copyright 2017 Evan Prodromou <legal@imfn.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')
const {parse} = require('url')

const env = require('./env')
const inboxBatch = require('./inbox-batch')
const validate = require('./validate')

const AS2 = 'https://www.w3.org/ns/activitystreams'
const AS2_MIME = 'application/activity+json'
const TAG = 'foo'

const id = `${env.TAGS_PUB_URL_ROOT}/tag/${TAG}`
const activity = {
  '@context': AS2,
  'id': 'http://localhost:8080/activity/create/1',
  'type': 'Create',
  'summary': `Evan created a note`,
  'to': ['https://www.w3.org/ns/activitystreams#Public'],
  'cc': [id],
  'actor': {
    'type': 'Person',
    'id': 'http://localhost:8080/person/evan',
    'name': 'Evan Prodromou'
  },
  'object': {
    'type': 'Note',
    'id': 'http://localhost:8080/person/evan/note/1',
    'content': `#${TAG}`,
    'tag': [{
      'id': id,
      'type': 'Hashtag',
      'name': `#${TAG}`
    }]
  }
}

vows.describe('Create activity')
  .addBatch(inboxBatch(activity, {
    'and we check the tag outbox': {
      async topic () {
        const root = `http://${env.TAGS_PUB_HOSTNAME}:${env.TAGS_PUB_PORT}`
        const url = `${root}/tag/${TAG}/outbox`
        return fetch(url, {'headers': {'Accept': AS2_MIME}})
      },
      'it works': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
        assert.equal(res.status, 200)
      },
      'and we examine the body': {
        async topic (res) {
          return res.json()
        },
        'it works': (err, json) => {
          assert.ifError(err)
          assert.isObject(json)
        },
        'it includes an Announce activity sharing the posted activity': (err, json) => {
          assert.ifError(err)
          assert.isObject(json)
          validate.document(json)
          assert.isObject(json.first)
          assert.isArray(json.first.items)
          assert.lengthOf(json.first.items, 1)
          validate.announce(json.first.items[0])
          assert.equal(json.first.items[0].object.id, activity.id)
        },
        'and we fetch the Announce activity': {
          async topic (outbox) {
            const [announce] = outbox.first.items
            const {id} = announce
            const parts = parse(id)
            const root = `http://${env.TAGS_PUB_HOSTNAME}:${env.TAGS_PUB_PORT}`
            const url = `${root}${parts.path}`
            return fetch(url, {'headers': {'Accept': AS2_MIME}})
          },
          'it works': (err, res) => {
            assert.ifError(err)
            assert.isObject(res)
            assert.equal(res.status, 200)
          },
          'and we examine the body': {
            async topic (res) {
              return res.json()
            },
            'it works': (err, json) => {
              assert.ifError(err)
              assert.isObject(json)
              validate.document(json)
              validate.announce(json)
              assert.equal(json.object.id, activity.id)
            }
          }
        }
      }
    }
  }))
  .export(module)
