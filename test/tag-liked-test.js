// tag-liked-test.js -- Test the /tag/:tag/liked endpoint
//
// Copyright 2017 Evan Prodromou <legal@imfn.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')
const debug = require('debug')('tags-pub:tag-liked-test')

const Server = require('./server')
const env = require('./env')

const AS2 = 'https://www.w3.org/ns/activitystreams'

const AS2_MIME = 'application/activity+json'
const TAG = 'foo'

vows.describe('/tag/:tag/liked endpoint')
  .addBatch(Server.batch(env, {
    'and we get the liked collection of a tag': {
      topic () {
        const root = `http://${env.TAGS_PUB_HOSTNAME}:${env.TAGS_PUB_PORT}`
        const url = `${root}/tag/${TAG}/liked`
        return fetch(url, {'headers': {'Accept': AS2_MIME}})
      },
      'it works': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
        assert.equal(res.status, 200)
      },
      'it has the right MIME type': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
        assert.equal(res.status, 200)
        assert.ok(res.headers.has('Content-Type'))
        assert.equal(res.headers.get('Content-Type'), AS2_MIME)
      },
      'and we get its JSON contents': {
        topic (res) {
          return res.json()
        },
        'it looks correct': (err, json) => {
          assert.ifError(err)
          debug(json)
          assert.isObject(json)
          assert.isString(json['@context'])
          assert.equal(json['@context'], AS2)
          assert.isString(json.type)
          assert.equal(json.type, 'Collection')
          assert.isString(json.id)
          assert.equal(json.id, `${env.TAGS_PUB_URL_ROOT}/tag/${TAG}/liked`)
          assert.isString(json.summary)
          assert.equal(json.summary, 'objects and activities liked by #foo')
          assert.isString(json.attributedTo)
          assert.equal(json.attributedTo, `${env.TAGS_PUB_URL_ROOT}/tag/${TAG}`)
          assert.isNumber(json.totalItems)
          assert.equal(json.totalItems, 0)
        }
      }
    }
  }))
  .export(module)
