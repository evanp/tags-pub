// inbox-batch.js -- boilerplate for tests that POST to /tag/:tag/inbox
//
// Copyright 2017 Evan Prodromou <legal@imfn.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const fs = require('fs')
const path = require('path')
const http = require('http')
const vows = require('perjury')
const {assert} = vows
const {sign} = require('http-signature')
const debug = require('debug')('tags-pub:post-tag-inbox-test')
const _ = require('lodash')

const Server = require('./server')
const env = require('./env')

const AS2 = 'https://www.w3.org/ns/activitystreams'
const AS2_MIME = 'application/activity+json'
const TAG = 'foo'

module.exports = (activity, tests = {}) => {
  const start = {
    'and we set up a remote server mock': {
      topic () {
        return new Promise((resolve, reject) => {
          debug('Reading pubkey')
          const pubkey = fs.readFileSync(path.join(__dirname, 'pubkey.pem'), {encoding: 'utf8'})
          debug('Creating body')
          const body = Buffer.from(JSON.stringify({
            '@context': AS2,
            'type': 'Person',
            'id': 'http://localhost:8080/person/evan',
            'name': 'Evan Prodromou',
            'publicKey': {'id': 'http://localhost:8080/person/evan#main-key',
              'owner': 'http://localhost:8080/person/evan',
              'publicKeyPem': pubkey
            }
          }))
          debug('Creating server')
          const remote = http.createServer((req, res) => {
            debug('Got request')
            res.writeHead(200, {
              'Content-Length': Buffer.byteLength(body),
              'Content-Type': AS2_MIME
            })
            res.end(body)
          })
          remote.once('error', (err) => {
            debug('Remote got an error')
            reject(err)
          })
          debug('Listening')
          remote.listen(8080, () => {
            debug('Confirmed')
            resolve(remote)
          })
          return remote
        })
      },
      'it works': (err, remote) => {
        assert.ifError(err)
        assert.isObject(remote)
      },
      teardown (remote) {
        debug('Remote teardown')
        return new Promise((resolve, reject) => {
          debug('resolving promise')
          remote.once('error', reject)
          remote.once('close', resolve)
          debug('closing remote')
          remote.close()
        })
      }
    }
  }

  const title = `and we post a ${activity.type} activity to the inbox collection of a tag`

  start['and we set up a remote server mock'][title] = {
    topic () {
      return new Promise((resolve, reject) => {
        const key = fs.readFileSync(path.join(__dirname, 'key.pem'), 'ascii')
        const body = activity
        const options = {
          method: 'POST',
          headers: {
            'Accept': AS2_MIME,
            'Content-Type': AS2_MIME
          },
          host: env.TAGS_PUB_HOSTNAME,
          port: Number(env.TAGS_PUB_PORT),
          path: `/tag/${TAG}/inbox`
        }
        const req = http.request(options)
        req.on('error', (err) => {
          reject(err)
        })
        req.on('response', (res) => {
          resolve(res)
        })
        sign(req, {key: key, keyId: 'http://localhost:8080/person/evan'})
        req.write(JSON.stringify(body), 'utf8')
        req.end()
      })
    },
    'it gives a 202 Accepted response': (err, res) => {
      assert.ifError(err)
      assert.isObject(res)
      assert.equal(res.statusCode, 202)
    }
  }
  if (_.keys(tests).length > 0) {
    const wait = {
      'and we wait 1 second': {
        async topic () {
          return new Promise((resolve, reject) => {
            const handler = () => {
              resolve()
            }
            setTimeout(handler, 1000)
          })
        },
        'it works': (err) => {
          assert.ifError(err)
        }
      }
    }

    _.extend(wait['and we wait 1 second'], tests)
    _.extend(start['and we set up a remote server mock'][title], wait)
  }
  return Server.batch(env, start)
}
